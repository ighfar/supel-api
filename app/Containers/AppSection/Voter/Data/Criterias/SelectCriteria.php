<?php

namespace App\Containers\AppSection\Voter\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class SelectCriteria extends Criteria
{
    protected $dapilKabupaten;
    protected $dapilKecamatan;
    protected $kodeKabupaten;
    protected $kodeKecamatan;
    protected $kodeDesa;
    protected $cari;

    public function __construct($dapilKabupaten, $dapilKecamatan, $kodeKabupaten, $kodeKecamatan, $kodeDesa, $cari)
    {
        $this->dapilKabupaten = $dapilKabupaten;
        $this->dapilKecamatan = $dapilKecamatan;
        $this->kodeKabupaten = $kodeKabupaten;
        $this->kodeKecamatan = $kodeKecamatan;
        $this->kodeDesa = $kodeDesa;
        $this->cari = $cari;
    }
  public function apply($model, PrettusRepositoryInterface $repository)
  {
      $model = $model->select([
          'id',
          'nkk',
          'nik',
          'name',
          'tempat_lahir',
          'tanggal_lahir',
          'kode_provinsi',
          'kode_kabupaten',
          'kode_kecamatan',
          'kode_desa',
          'alamat',
      ]);

      if ($this->dapilKabupaten != null && $this->kodeKabupaten == null) {
          $model = $model->whereIn('kode_kabupaten', $this->dapilKabupaten);
      }

      if ($this->dapilKecamatan != null && $this->kodeKecamatan == null) {
          $model = $model->whereIn('kode_kecamatan', $this->dapilKabupaten);
      }

      if ($this->kodeKabupaten != null) {
          $model = $model->where('kode_kabupaten', $this->kodeKabupaten);
      }

      if ($this->kodeKecamatan != null) {
          $model = $model->where('kode_kecamatan', $this->kodeKecamatan);
      }

      if ($this->cari != null) {
          $model = $model->where('name', $this->cari . '%')->orWhere('nkk', $this->cari . '%')->orWhere('nik', $this->cari . '%');
      }

      var_dump('dd');
      die($model->toSql());

      return $model->limit(100);

  }
}
